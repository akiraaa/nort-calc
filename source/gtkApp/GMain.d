/**
 * Nort-Calc
 * gitlab.com/akiraaa
 */
//--MODULE--------------------------------
module gtkApp.GMain;
//--GTKD----------------------------------
import gtk.Button;
import gtk.Builder;
import gtk.Widget;
import gtk.Window;
import gtk.Main;
import gtk.Menu;
import gtk.MenuItem;
import gtk.MenuBar;
import gobject.Type;
//--SELFI----------------------------------
import gtkApp.ButtonEvents;
import gtkApp.WindowEvents;
import gtkApp.GForm;
//--MARS----------------------------------
import std.conv;
import std.file;
import std.json;
import std.stdio;
import std.datetime;
import std.algorithm.sorting;
//--Utils---------------------------------
import gtkApp.util;
import coingecko;

class GMain{
    private Builder builder;
    private Window  window;

    /**
     * 파일에서 폼 생성
     */
    this(string glade_file_path, string window_id, string[] args){
        if(exists("db") == false){
            mkdir("db");
        }

        // 핑 체크
        coinGeckoV3 client = new coinGeckoV3();
        if(client.ping() == true){
            
            // 프로그램 실행 시 가격데이터 갱신
            priceData data = new priceData();
            data.updatePrice();

            Main.init(args);
            this.builder = new Builder();
            builder.addFromFile(glade_file_path);
            this.window = cast(Window)this.builder.getObject(window_id);
        }

    }

    /**
     * 문자열에서 폼 생성
     */
    this(string window_id, string[] args){
        if(exists("db") == false){
            mkdir("db");
        }

        // 핑 체크
        coinGeckoV3 client = new coinGeckoV3();
        if(client.ping() == true){
            
            // 프로그램 실행 시 가격데이터 갱신
            priceData data = new priceData();
            data.updatePrice();

            Main.init(args);
            this.builder = new Builder();

            // 폼 문자열
            MainForm mainForm = MainForm();
            builder.addFromString(mainForm.getXML());
            this.window = cast(Window)this.builder.getObject(window_id);
        }

    }

    void loadEvent(){
        ButtonEvents be = new ButtonEvents(this.builder);
        be.addEvents();
        WindowEvents we = new WindowEvents();
        we.addEvents(this.builder);

        this.window.addOnHide( delegate void(Widget widget){ Main.quit(); } );
    }

    void show(){
        this.builder.connectSignals(null);
        this.window.showAll();
	    Main.run();
    }
}