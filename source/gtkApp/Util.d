/**
 * Nort-Calc
 * gitlab.com/akiraaa
 */
module gtkApp.util;
//--MARS----------------------------------
import std.conv;
import std.file;
import std.json;
import std.stdio;
import std.datetime;
import std.algorithm.sorting;
//--Utils---------------------------------
import coingecko;

// 가격데이터 관리
class priceData{
    private coinGeckoV3 client;

    ///
    /// 생성자
    ///
    this(){
        client = new coinGeckoV3();
    }

    ///
    /// 마지막 가격 데이터 파일명 리턴
    ///
    private string getlastFileName(string path){
        string[] db_list = null;
        // 디렉토리 내 파일 리스트 얻고 string[]으로 변환
        foreach(e; dirEntries(path, SpanMode.depth)){
            db_list ~= e;
        }

        // 리버스 정렬 후 맨 첫번째 값 리턴
        db_list.sort!("a > b");
        return db_list[0];
    }

    ///
    /// 마지막 가격 데이터 파일을 읽어 리턴
    ///
    string[string] getLastData(){
        string file_name = getlastFileName("./db");
        File f = File(file_name, "r");

        string json_str = "";
        while(!f.eof()){
            json_str ~= f.readln();
        }

        // JSON 데이터로 가져오고
        JSONValue json_obj = parseJSON(json_str);

        // 데이터로 파싱
        string[string] data = [
              "KRW" : to!string(json_obj["KRW"])
            , "JPY" : to!string(json_obj["JPY"])
            , "CNY" : to!string(json_obj["CNY"])
            , "USD" : to!string(json_obj["USD"])
        ];

        return data;
    }

    ///
    /// 가격데이터를 불러와 파일로 저장
    ///
    void updatePrice(){
        // 가격데이터 저장
        JSONValue json_obj = [
              "KRW": client.simplePrice("northern", "krw")
            , "JPY": client.simplePrice("northern", "jpy")
            , "CNY": client.simplePrice("northern", "cny")
            , "USD": client.simplePrice("northern", "usd")
        ];
        
        string serial = json_obj.toString();

        SysTime now = Clock.currTime();
        string file_name = to!string(now.stdTime());

        File f = File("./db/"~file_name, "w");
        f.write(serial);
        f.close();
    }
}
