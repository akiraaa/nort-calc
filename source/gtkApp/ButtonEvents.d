/**
 * Nort-Calc
 * gitlab.com/akiraaa
 */
//--MODULE--------------------------------
module gtkApp.ButtonEvents;
//--GTKD----------------------------------
import gtk.Builder;
import gtk.Entry;
import gtk.ComboBoxText;
//import gobject.Type;
//--SELFI----------------------------------
import gtkApp.GMain;
//--MARS-----------------------------------
import std.uni;
import std.conv;
import std.regex;
import std.stdio;
import std.string;
//--Utils---------------------------------
import gtkApp.util;
import coingecko;

Builder comBuilder;

class ButtonEvents{
    Builder builder;
    this(ref Builder builder){
        this.builder = builder;
    }
    void addEvents(){
        comBuilder = this.builder;
        this.builder.addCallbackSymbol("on_COMBO_changed",      &on_COMBO_changed);
        this.builder.addCallbackSymbol("on_NORT_activate",      &on_NORT_activate);
        this.builder.addCallbackSymbol("on_refreshBtn_clicked", &on_refreshBtn_clicked);
    }
}
extern(C) void on_COMBO_changed(){
    auto combo      = cast(ComboBoxText)comBuilder.getObject("COMBO");
    auto vscur_text = cast(Entry)comBuilder.getObject("VSCUR");
    auto nort_text  = cast(Entry)comBuilder.getObject("NORT");

    string ticker = combo.getActiveText().toUpper;
    
    // 가격데이터 불러오기
    priceData data = new priceData();
    string[string] price_map = data.getLastData();

    // 입력한 NORT 수량 불러오기
    string how_many = nort_text.getText();
    
    // 정규식으로 가장 첫번째 숫자만 가져온다
    if(isNumeric(nort_text.getText())){
        float p = to!float(nort_text.getText()) * to!float(price_map[ticker]);
        vscur_text.setText(to!string(p));
    }


}

extern(C) void on_NORT_activate(){
    auto combo      = cast(ComboBoxText)comBuilder.getObject("COMBO");
    auto vscur_text = cast(Entry)comBuilder.getObject("VSCUR");
    auto nort_text  = cast(Entry)comBuilder.getObject("NORT");

    // 가격데이터 불러오기
    string ticker = combo.getActiveText().toUpper;
    priceData data = new priceData();
    string[string] price_map = data.getLastData();

    if(isNumeric(nort_text.getText())){
        float amount = to!float(nort_text.getText());
        float price  = to!float(price_map[ticker]);
        vscur_text.setText(to!string(amount*price));   
    }
}
extern(C) void on_refreshBtn_clicked(){
    // 가격 데이터 갱신
    priceData data = new priceData();
    data.updatePrice();

    auto combo      = cast(ComboBoxText)comBuilder.getObject("COMBO");
    auto vscur_text = cast(Entry)comBuilder.getObject("VSCUR");
    auto nort_text  = cast(Entry)comBuilder.getObject("NORT");

    string ticker = combo.getActiveText().toUpper;
    
    // 가격데이터 불러오기
    string[string] price_map = data.getLastData();

    // 수량을 1개로 초기화
    nort_text.setText("1");

    // 표시 가격갱신
    
    // 정규식으로 가장 첫번째 숫자만 가져온다
    float p = to!float(price_map[ticker]);
    vscur_text.setText(to!string(p));
}
