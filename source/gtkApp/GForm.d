/**
 * Nort-Calc
 * gitlab.com/akiraaa
 */
//--MODULE--------------------------------
module gtkApp.GForm;

struct MainForm{
    private string XML = `<?xml version="1.0" encoding="UTF-8"?>
<!-- Generated with glade 3.20.0 -->
<interface>
  <requires lib="gtk+" version="3.20"/>
  <object class="GtkWindow" id="window1">
    <property name="can_focus">False</property>
    <child>
      <object class="GtkBox">
        <property name="visible">True</property>
        <property name="can_focus">False</property>
        <property name="orientation">vertical</property>
        <child>
          <object class="GtkBox">
            <property name="visible">True</property>
            <property name="can_focus">False</property>
            <child>
              <object class="GtkLabel">
                <property name="visible">True</property>
                <property name="can_focus">False</property>
                <property name="xpad">10</property>
                <property name="ypad">10</property>
                <property name="label" translatable="yes">NORT</property>
                <attributes>
                  <attribute name="weight" value="bold"/>
                </attributes>
              </object>
              <packing>
                <property name="expand">False</property>
                <property name="fill">True</property>
                <property name="position">0</property>
              </packing>
            </child>
            <child>
              <object class="GtkEntry" id="NORT">
                <property name="visible">True</property>
                <property name="can_focus">True</property>
                <property name="activates_default">True</property>
                <property name="text" translatable="yes">1</property>
                <property name="caps_lock_warning">False</property>
                <signal name="activate" handler="on_NORT_activate" swapped="no"/>
              </object>
              <packing>
                <property name="expand">False</property>
                <property name="fill">True</property>
                <property name="position">1</property>
              </packing>
            </child>
            <child>
              <object class="GtkLabel">
                <property name="visible">True</property>
                <property name="can_focus">False</property>
                <property name="xpad">10</property>
                <property name="label" translatable="yes">:</property>
              </object>
              <packing>
                <property name="expand">False</property>
                <property name="fill">True</property>
                <property name="position">2</property>
              </packing>
            </child>
            <child>
              <object class="GtkComboBoxText" id="COMBO">
                <property name="visible">True</property>
                <property name="can_focus">False</property>
                <property name="active">0</property>
                <items>
                  <item id="CUR_KRW" translatable="yes">KRW</item>
                  <item id="CUR_JPY" translatable="yes">JPY</item>
                  <item id="CUR_CNY" translatable="yes">CNY</item>
                  <item id="CUR_USD" translatable="yes">USD</item>
                </items>
                <signal name="changed" handler="on_COMBO_changed" swapped="no"/>
              </object>
              <packing>
                <property name="expand">False</property>
                <property name="fill">True</property>
                <property name="position">3</property>
              </packing>
            </child>
            <child>
              <object class="GtkEntry" id="VSCUR">
                <property name="visible">True</property>
                <property name="can_focus">True</property>
                <property name="editable">False</property>
                <property name="input_purpose">number</property>
              </object>
              <packing>
                <property name="expand">False</property>
                <property name="fill">True</property>
                <property name="position">4</property>
              </packing>
            </child>
          </object>
          <packing>
            <property name="expand">False</property>
            <property name="fill">True</property>
            <property name="position">0</property>
          </packing>
        </child>
        <child>
          <object class="GtkButton" id="refreshBtn">
            <property name="label" translatable="yes">Refresh</property>
            <property name="visible">True</property>
            <property name="can_focus">True</property>
            <property name="receives_default">True</property>
            <signal name="clicked" handler="on_refreshBtn_clicked" swapped="no"/>
          </object>
          <packing>
            <property name="expand">False</property>
            <property name="fill">True</property>
            <property name="position">1</property>
          </packing>
        </child>
        <child>
          <object class="GtkLabel">
            <property name="visible">True</property>
            <property name="can_focus">False</property>
            <property name="label" translatable="yes">From CoinGecko / gitlab.com/akiraaa</property>
          </object>
          <packing>
            <property name="expand">False</property>
            <property name="fill">True</property>
            <property name="position">2</property>
          </packing>
        </child>
      </object>
    </child>
  </object>
</interface>
`;

    string getXML(){
        return this.XML;
    }
}